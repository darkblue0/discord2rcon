# Discord2Rcon Bot
A simple discord rcon bot for xonotic

## Dependencies
- discord.py (`pip3 install discord` or `sudo apt install python3-discord`)

## How to run
python3 /path/to/discord2rcon.py /path/to/config.json

## Examples
`!help`

![helpcmd](https://i.ibb.co/Vg5jxDW/1.png)

`!rcon dm g_maplist`

![g_maplist](https://i.ibb.co/z8M0Mq7/2.png)
