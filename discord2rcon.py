"""
MIT License

Copyright (c) 2023 DarkBlue

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import sys
import math
import json
import discord
import re
from xrcon.client import XRcon

MAX_CONTENT_LENGTH = 2000 # Discord message length limit

if len(sys.argv) != 2:
    sys.exit("Usage: discord2rcon.py CONFIG_FILE")

with open(sys.argv[1], "r") as f: config = json.load(f)

botConfig = config["bot"]
serversConfig = config["servers"]
for server in serversConfig:
    rconPass = ""
    if "authFile" in server:
        if server["authFile"]:
            with open(server["authFile"], 'r') as fp:
                lines = fp.readlines()
                for line in lines:
                    if line.startswith("rcon_password"):
                        rconPass = line.split("//", 1)[0]
                        rconPass = rconPass[len("rcon_password"):].replace("\"", "").strip()
                        break
    elif "authPass" in server:
        rconPass = server["authPass"]
        
    if rconPass == "":
        print(f'Warning: {server["prefix"]} Empty rcon pass')

    server["rconPass"] = rconPass

class Discord2Rcon(discord.Client):
    def parse_status(self, target, statusList):
        for info in statusList:
            if info.lower().startswith(target.lower()):
                return info[len(target):].strip()
    async def on_ready(self):
        print(f'Logged in as {self.user} (ID: {self.user.id})')
        print('------')
        for channel in botConfig["botChannels"]:
            channel = client.get_channel(channel)
            await channel.send('**:robot: Rcon bot connected**')

    async def on_message(self, message):
        # we do not want the bot to reply to itself
        if message.author.id == self.user.id:
            return

        isChannelAllowed = False
        for channel in botConfig["botChannels"]:
            if channel == message.channel.id:
                isChannelAllowed = True
                break

        if isChannelAllowed == False:
            return

        if message.content.startswith('!restart'):
            isAuthorized = False
            for userRole in message.author.roles:
                if(isAuthorized):
                    break

                for botRole in botConfig["roles"]:
                    if userRole.name.lower() == botRole.lower():
                        isAuthorized = True
                        break

            if isAuthorized:
                await message.reply('**Restarting Rcon Bot...**', mention_author=False)
                await self.close()
                exit()
            else:
                await message.reply('**Unauthorized**', mention_author=False);
        elif message.content.startswith('!help'):
            helpMessage = "Usage: !rcon `-arguments` `prefix` `commands`\n"
            helpMessage += "arguments: `-q` Quite mode. No output report (only errors are reported)\n"
            helpMessage += "commands: A single command, or a list of commands separated by a semi-colon\n"
            helpMessage += "You are authorized to use the following prefixes:\n"

            for userRole in message.author.roles:
                for server in serversConfig:
                    for role in server["roles"]:
                        if role.lower() == userRole.name.lower():
                            helpMessage += f'`{server["prefix"]}` -> **{server["serverName"]}**, Port: `{server["port"]}`\n'

            await message.author.send(helpMessage)
        elif message.content.startswith('!status'):
            requestedList = serversConfig
            prefixes = message.content.split(" ")
            if len(prefixes) > 1:
                requestedList = []

                # Skip the command part '!status'
                prefixes = prefixes[1:]

                addedServers = []
                noServerPrefix = []
                for i in range(len(prefixes)):
                    prefix = prefixes[i]
                    for j in range(len(serversConfig)):
                        server = serversConfig[j]
                        if prefix.lower() == server["prefix"].lower():
                            if j in addedServers:
                                break
                            requestedList.append(server)
                            addedServers.append(j)
                            break
                        elif j == len(serversConfig)-1 and prefix.lower() not in noServerPrefix:
                                noServerPrefix.append(prefix.lower())

                if len(noServerPrefix):
                    await message.channel.send(f'**Unknown prefixes: `{", ".join(noServerPrefix)}`**')

            for server in requestedList:
                rcon = XRcon(server["host"], server["port"], server["rconPass"])
                rcon.connect()

                try:
                    out = rcon.execute("status")
                    statusList = bytes.decode(out).split("\n")
                    serverName = self.parse_status("host:", statusList)
                    serverCPU = self.parse_status("timing:", statusList)
                    serverPlayers = self.parse_status("players:", statusList)
                    serverMap = self.parse_status("map:", statusList)

                    embed = discord.Embed(colour=0xFF0000, title=f'Server status {serverName}: ')
                    embed.add_field(name="CPU load:", value=f'`{serverCPU}`', inline=False)
                    embed.add_field(name="Players:", value=f'`{serverPlayers}`', inline=True)
                    embed.add_field(name="Map:", value=f'`{serverMap}`', inline=True)

                    await message.channel.send(embed=embed)
                except BaseException as error:
                    for channel in botConfig["botChannels"]:
                        print(error)
                        channel = client.get_channel(channel)
                        await channel.send('**An exception occurred: {}**'.format(error))
                finally:
                    rcon.close()
        elif message.content.startswith('!rcon '):
            parsedMessage = message.content.split(" ", 2)[1:]
            quiteMode = False

            if(parsedMessage[0].startswith("-")):
                botArguments = parsedMessage[0]

                for letter in botArguments[1:]:
                    if letter == 'q':
                        quiteMode = True

                # Skip bot arguments
                parsedMessage = parsedMessage[1].split(" ", 1)

            rconServer = None
            for server in serversConfig:
                if server["prefix"] == parsedMessage[0]:
                    rconServer = server
                    break

            if rconServer is None:
                await message.reply(f'**Found no servers with the prefix `{parsedMessage[0]}`**', mention_author=False)
                return

            isAuthorized = False
            for userRole in message.author.roles:
                if(isAuthorized):
                    break

                for serverRole in rconServer["roles"]:
                    if userRole.name.lower() == serverRole.lower():
                        isAuthorized = True
                        break

            if not isAuthorized:
                await message.reply('**Unauthorized. Type `!help` for usage**', mention_author=False);
                return

            # Support for multiple commands (separated by semi colon)
            parsedMessage[1] = parsedMessage[1].split(";")

            if len(parsedMessage[1]) == 0:
                return

            rcon = XRcon(rconServer["host"], rconServer["port"], rconServer["rconPass"])
            rcon.connect()

            try:
                for cmd in parsedMessage[1]:
                    result = rcon.execute(cmd)
                    decodedResult = bytes.decode(result)
                    decodedResult = re.sub("(?:\^x(?:[0-9]|[A-F])(?:[0-9]|[A-F])(?:[0-9]|[A-F])|\^[0-9])", "", decodedResult, flags=re.IGNORECASE)

                    print(f'Sending rcon command `{cmd}` to {rconServer["prefix"]}:{rconServer["port"]}, recieved from {message.author.name}:{message.author.id} on channel {message.channel.name}:{message.channel.id}')

                    if quiteMode == False:
                        resultChunks = []
                        i = math.ceil(len(decodedResult) / MAX_CONTENT_LENGTH)
                        j = 0
                        while i - j > 0:
                            # Last chunk
                            if j == i - 1:
                                if decodedResult[j * MAX_CONTENT_LENGTH:] != '':
                                    resultChunks.append(decodedResult[j * MAX_CONTENT_LENGTH:])
                                break;

                            resultChunks.append(decodedResult[j * MAX_CONTENT_LENGTH:j * MAX_CONTENT_LENGTH + MAX_CONTENT_LENGTH])
                            j = j+1

                        await message.channel.send(f'**`{cmd}`**:\n')
                        for chunk in resultChunks:
                            await message.channel.send(chunk)

                    print(f'Result: {decodedResult}')
            except BaseException as error:
                for channel in botConfig["botChannels"]:
                    print(error)
                    channel = client.get_channel(channel)
                    await channel.send('**An exception occurred: {}**'.format(error))
            finally:
                rcon.close()

intents = discord.Intents.default()
intents.message_content = True
client = Discord2Rcon(intents=intents)
client.run(botConfig["token"])

